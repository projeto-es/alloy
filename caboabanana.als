abstract sig Id { }

sig UserId, GroupId extends Id { }

sig User {
    id: one UserId,
    groups: set GroupUser
}

sig Group {
    id: one GroupId,
    authorId: one UserId,
    members: some GroupUser
}

sig GroupUser {
	userId: one UserId,
	groupId: one GroupId
}

fact idDoesNotExistWithoutInstance {
	all uId: UserId | one uId.~id
	all gId: GroupId | one gId.~id
}

fun GroupUser.id : UserId -> GroupId {
    this.userId -> this.groupId
}

fact primaryKeysAreUnique {
    all u, u': User | u != u' implies u.id != u'.id
    all g, g': Group | g!= g' implies g.id != g'.id
	all gU, gU': GroupUser | gU != gU' implies gU.id != gU'.id
}

pred Group.authorIsAMember {
	this.authorId in this.members.userId
}

fact groupAuthorIsAMember {
    all g: Group | one u: User | u.id = g.authorId and g.authorIsAMember
}

assert disjointIds {
	Id = UserId + GroupId and disj [UserId, GroupId]
}

assert groupUserExistingReferences {
    all gU: GroupUser | one gU.userId.~id and one gU.groupId.~id
}

fact userIsAGroupMember {
	all u: User, g: Group |  g.id in u.groups.groupId iff u.id in g.members.userId
}

check userIsAGroupMember for 5
